## Aer Lingus automatic booking script

### Description

aerlingus.com automatic booking script.  
Written by Daniel Barrington.

### Configuration

Edit the `details.js` file to change airports, passenger details, payments details, etc.

### To Run

`phantomjs aerLingus.js`  
Turn on the `saveScreenshot` flag in `aerLingus.js` to save a screenshot every second.

