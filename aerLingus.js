var page = require('webpage').create();

var url = require('./details').url;
var details = require('./details').details;

var saveScreenshots = false;	// enable to save a screen shot every second.

var pages = [page1, page2, page3, page4, page5];
var buttons = ['button[data-test-id="test_continue_btn"]', '#test_continueTravelEsstl', 'button[data-test-id="test_continue_btn"]', '#continue-btn', 'button[test-data-id="test_complete_purchase"]'];
var pageNum = 0;


// pageHandler function checks if the submit button exists
// loops until page has fully loaded if button does not exist
// fills in any necessary forms and submits

// *Not working as expected so incorporated loop into each page function instead
// not pretty but works!

// function pageHandler(details, button, pageInstructions) {
// 	page.evaluate(function(details, button, pageInstructions) {
// 		var count = 0;
// 		function loop() {
// 			if(document.querySelector(button)) {
// 				pageInstructions();
// 			} else {
// 				setTimeout(loop, 1000);
// 			}
// 			count++;
// 			console.log('loop ' + count);
// 		}
// 		loop();
// 	}, details, button, pageInstructions);
// }


// every time the URL changes handle then next page
page.onUrlChanged = function(url) {
	console.log('url:', url);
	if(url === 'about:blank') { 
		return;
	}
	if(typeof pages[pageNum] !== 'undefined') {
		// pageHandler(details, buttons[pageNum], pages[pageNum](page));
		pages[pageNum](page);
		pageNum++;
	} else {
		page.render('finished.png');
		phantom.exit();
	}
};

// print browser console to command line
page.onConsoleMessage = function(msg) {
  console.log('CONSOLE: ' + msg);
};

// render screen shot before submitting each page
page.onCallback = function(str) {
	page.render(str+'.png');

	if(str === 'finished') {
		phantom.exit();
	}
}

// screenshot every second
var sCount = 0;
function screenshot() {
	page.render(sCount+".png");
	sCount++;
	setTimeout(screenshot, 1000);
};
if(saveScreenshots) { screenshot(); }


// instructions for each page

// Flight select page
// Cheapest flights selected by default
function page1() {
	page.evaluate(function(buttons) {
		function loop() {
			console.log('page1');
			if(document.querySelector(buttons[0])) {
				$(buttons[0]).click();
			} else {
				setTimeout(loop, 1000);
			}
		}
		loop();
	}, buttons);
}


// Passenger information
function page2() {
	page.evaluate(function(buttons, details) {
		function loop() {
			console.log('page2');
			if(document.querySelector(buttons[1])) {
				$('#test_adultTitle-0-1').val(details.adult.title).change();
				$('#test_adultFname-0-1').val(details.adult.fName).change();
				$('#test_adultLname-0-2').val(details.adult.lName).change();
				$('#test_email-5, #test_confirmemail-8').val(details.adult.email).change();
				$('#field_test_email-receive').prop('checked', false);
				$('#test_acode-6').val(details.adult.areaCode).change();
				$('#test_mobnumber-7').val(details.adult.localNumber).change();

				$('#test_childTitle-0-2').val(details.child.title).change();
				$('#test_childFname-0-3').val(details.child.fName).change();
				$('#test_childLname-0-4').val(details.child.lName).change();

				$(buttons[1]).click();
			} else {
				setTimeout(loop, 1000);
			}
		}
		loop();
	}, buttons, details);
}

// Seat select and bag checkin
function page3() {
	page.evaluate(function(buttons) {
		function loop() {
			console.log('page3');
			if(document.querySelector(buttons[2])) {
				$(buttons[2]).click();

				$('button[data-test-id="test_continue_without_bags_btn"]').click();
			} else {
				setTimeout(loop, 1000);
			}
		}
		loop();
	}, buttons);
}

// Extras
function page4() {
	page.evaluate(function(buttons) {
		function loop() {
			console.log('page4');
			if(document.querySelector(buttons[3])) {
				$(buttons[3]).click();
			} else {
				setTimeout(loop, 1000);
			}
		}
		loop();
	}, buttons);
}

// Payment
function page5() {
	page.evaluate(function(buttons, details) {
		function loop() {
			console.log('page5');
			if(document.querySelector(buttons[4])) {
				$('#ccCode-1').val(details.card.type).change();
				$('#test_cardNumber-6').val(details.card.number).change();

				$('#test_expiryDate-2').val(details.card.expireMonth).change();
				$('#test_expiryYear-3').val(details.card.expireYear).change();
				$('#test_fname-1').val(details.card.fName).change();
				$('#test_lname-2').val(details.card.lName).change();
				$('#test_addLineOne-7').val(details.card.address).change();
				$('#test_cityName-3').val(details.card.city).change();

				var countries = $('#test_countryName-4 option');
				for(var i = 0; i < countries.length; i++) {
					if(details.card.country === $(countries[i]).text()) {
						$('#test_countryName-4').val($(countries[i]).val()).change();
					}
				}

				// Form doesn't always fill on first attempt so try again.
				if($('#ccCode-1').val() === null) {
					console.log('form not ready, trying again...');
					setTimeout(loop, 1000);
				} else {
					$('label[test-data-id="test_accept_terms"]').click();

					// button takes a moment to become active
					setTimeout(function() { $(buttons[4]).click(); }, 100); 

					if (typeof window.callPhantom === 'function') {
						window.callPhantom('finished');
					}
				}


			} else {
				setTimeout(loop, 1000);
			}
		}
		loop();
	}, buttons, details);
}

page.open(url, function(status) {
	console.log(status);
});

