var details = {
	adult: {
		title: 0, // ['Mr', 'Miss', 'Ms', 'Mrs', 'Dr']
		fName: 'Clancy',
		lName: 'Wiggum',
		email: 'test@test.com',
		countryCode: '+44',
		areaCode: 1234,
		localNumber: 123456
	},
	child: {
		title: 0,	// [Mr, Miss]
		fName: 'Ralph',
		lName: 'Wiggum'
	},
	card: {
		type: 0, // [Laser, Visa Card, MasterCard, American Express, Diners Club, Universal Air Travel Card, Maestro/Switch/Solo, Delta, Visa Electron]
		number: 4242424242424242,
		expireMonth: 0,
		expireYear: 0, // [2017, ...]

		fName: 'Clancy',
		lName: 'Wiggum',
		address: '123 Fake Street',
		city: 'London',
		country: 'United Kingdom'
	}
};

var source = 'LGW';
var destination = 'DUB';
var dateDepart = '2017-04-08';
var fareType = 'RETURN';
var dateReturn = '2017-04-12';
var adults = 1;
var youngAdults = 0;
var children = 1;
var infants = 0;

var url = "https://www.aerlingus.com/html/flightSearchResult.html#/groupBooking=false&fareCategory=ECONOMY&promoCode=";
	url += "&sourceAirportCode_0=" + source
	url += "&destinationAirportCode_0=" + destination
	url += "&departureDate_0=" + dateDepart
	url += "&numAdults=" + adults
	url += "&numChildren=" + children
	url += "&numInfants=" + infants
	url +="&numYoungAdults=" + youngAdults;

if(fareType === "RETURN") {
	url += "&fareType=" + fareType;
	url += "&sourceAirportCode_1=" + destination;
	url += "&destinationAirportCode_1=" + source;
	url += "&departureDate_1=" + dateReturn;
} else {
	url += "&fareType=ONEWAY";
}

exports.url = url;
exports.details = details;